﻿using AutoFixture;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Validators.PartnerValidatorTests
{
    public class ValidatePartnerGetByIdAsyncTests
    {
        [Fact]
        public void ValidatePartnerGetById_PartnerIsInDatabase_ReturnsVoid()
        {
            //Arrange
            Partner partner = PartnerBuilder.CreatePartner();
            Guid id = partner.Id;

            //Act
            var result = PartnerValidator.ValidatePartnerGetByIdAsync(id, partner);

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void ValidatePartnerGetById_PartnerIsNotInDatabase_ThrowsNotFoundException()
        {
            //Arrange
            Partner partner = null;
            Guid id = Guid.Parse("feea30da-eae7-473e-909a-0f0be40d78b2");

            //Assert
            var ex = Assert.Throws<NotFoundException>(() => PartnerValidator.ValidatePartnerGetByIdAsync(id, partner));
            ex.Message.Should().Be($"Объект с идентификатором: {id} не найден");
        }

        [Fact]
        public void ValidatePartnerGetById_PartnerIsNotActive_ThrowsBadRequestException()
        {
            //Arrange
            Fixture fixture = new Fixture();
            Partner partner = fixture.Build<Partner>().Without(p => p.PartnerLimits).With(p => p.IsActive, false).Create();
            Guid id = Guid.Parse("feea30da-eae7-473e-909a-0f0be40d78b2");

            //Assert
            var ex = Assert.Throws<BadRequestException>(() => PartnerValidator.ValidatePartnerGetByIdAsync(id, partner));
            ex.Message.Should().Be($"Партнер с идентификатором: {partner.Id} заблокирован");
        }
    }
}
