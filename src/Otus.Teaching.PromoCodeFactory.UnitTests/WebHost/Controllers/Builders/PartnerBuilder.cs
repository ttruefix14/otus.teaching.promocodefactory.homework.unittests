﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreatePartner()
        {
            Partner partner = new Partner()
            {
                Id = Guid.Parse("4a07c26e-332b-48b3-909d-bd87407823d6"),
                Name = "TestName",
                NumberIssuedPromoCodes = 0,
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            return partner;
        }

        public static Partner SetNotActive(this Partner partner)
        {
            partner.IsActive = false;
            return partner;
        }  

        public static Partner SetNumberIssuedPromoCodes(this Partner partner, int numberIssuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;
            return partner;
        }        
                
        public static Partner AddActivePartnerPromoCodeLimit(this Partner partner)
        {
            partner.PartnerLimits.Add(PartnerPromoCodeLimitBuilder.CreatePartnerPromoCodeLimit());
            return partner;
        }        
        
        public static Partner AddCanceledPartnerPromoCodeLimit(this Partner partner)
        {
            partner.PartnerLimits.Add(PartnerPromoCodeLimitBuilder.CreatePartnerPromoCodeLimit()
                .SetCancelDate(DateTime.Now));
            return partner;
        }  
    }
}
