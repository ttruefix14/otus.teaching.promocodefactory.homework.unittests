﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Builders
{
    public static class PartnerPromoCodeLimitBuilder
    {
        public static PartnerPromoCodeLimit CreatePartnerPromoCodeLimit()
        {
            PartnerPromoCodeLimit partnerPromoCodeLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("cc32b56e-1526-47d3-910e-4a8e20c36eb9"),
                PartnerId = Guid.Parse("4a07c26e-332b-48b3-909d-bd87407823d6"),
                Partner = PartnerBuilder.CreatePartner(),
                CreateDate = DateTime.Now,
                CancelDate = null,
                EndDate = DateTime.Parse("2024/03/25"),
                Limit = 1
            };
            return partnerPromoCodeLimit;
        }

        public static PartnerPromoCodeLimit SetCancelDate(this PartnerPromoCodeLimit partnerPromoCodeLimit, DateTime cancelDate)
        {
            partnerPromoCodeLimit.CancelDate = cancelDate;
            return partnerPromoCodeLimit;
        }

        public static SetPartnerPromoCodeLimitRequest CreateSetPartnerPromoCodeLimitRequest()
        {
            SetPartnerPromoCodeLimitRequest setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Parse("2024/03/25"),
                Limit = 1
            };
            return setPartnerPromoCodeLimitRequest;
        }

        public static SetPartnerPromoCodeLimitRequest SetZeroLimit(this SetPartnerPromoCodeLimitRequest setPartnerPromoCodeLimitRequest)
        {
            setPartnerPromoCodeLimitRequest.Limit = 0;
            return setPartnerPromoCodeLimitRequest;
        }
    }
}
