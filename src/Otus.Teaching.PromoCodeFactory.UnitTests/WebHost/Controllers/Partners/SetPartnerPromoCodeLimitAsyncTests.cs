﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            IFixture fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = null;
            SetPartnerPromoCodeLimitRequest request = null;

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive = false в классе Partner, 
        /// то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arrange
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = PartnerBuilder.CreatePartner()
                .SetNotActive();
            SetPartnerPromoCodeLimitRequest request = null;

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        ///Если партнеру выставляется лимит, то мы 
        ///должны обнулить количество промокодов, которые партнер выдал, если лимит не закончился
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_NullifyNumberIssuedPromoCodes()
        {
            //Arrange
            int numberIssuedPromoCodeLimit = 1;
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = PartnerBuilder.CreatePartner()
                .SetNumberIssuedPromoCodes(numberIssuedPromoCodeLimit)
                .AddActivePartnerPromoCodeLimit();
            SetPartnerPromoCodeLimitRequest request = PartnerPromoCodeLimitBuilder.CreateSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        ///Если партнеру выставляется лимит, то мы 
        ///не должны обнулять количество промокодов, которые партнер выдал, если лимит закончился
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasNoActiveLimit_DoNotNullifyNumberIssuedPromoCodes()
        {
            //Arrange
            int numberIssuedPromoCodeLimit = 1;
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = PartnerBuilder.CreatePartner()
                .SetNumberIssuedPromoCodes(numberIssuedPromoCodeLimit)
                .AddCanceledPartnerPromoCodeLimit();
            SetPartnerPromoCodeLimitRequest request = PartnerPromoCodeLimitBuilder.CreateSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        /// <summary>
        ///Если партнеру выставляется лимит, то мы 
        ///не должны обнулять количество промокодов, которые партнер выдал, если лимит закончился
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_CancelActivePartnerPromoCodeLimit()
        {
            //Arrange
            int numberIssuedPromoCodeLimit = 1;
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = PartnerBuilder.CreatePartner()
                .SetNumberIssuedPromoCodes(numberIssuedPromoCodeLimit)
                .AddActivePartnerPromoCodeLimit();
            SetPartnerPromoCodeLimitRequest request = PartnerPromoCodeLimitBuilder.CreateSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            partner.PartnerLimits.FirstOrDefault(p => p.CancelDate.HasValue).Should().NotBeNull();
        }
        
        /// <summary>
        ///Лимит должен быть выше нуля
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsEqualOrLowerThanZero_ReturnsBadRequest()
        {
            //Arrange
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = PartnerBuilder.CreatePartner();
            SetPartnerPromoCodeLimitRequest request = PartnerPromoCodeLimitBuilder.CreateSetPartnerPromoCodeLimitRequest()
                .SetZeroLimit();

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        ///Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerPromoCodeLimitIsValid_PartnerPromoCodeLimitSavedInDatabase()
        {
            //Arrange
            Guid id = Guid.Parse("6e16b4ad-020f-4da3-89f0-bb48e0ad7722");
            Partner partner = PartnerBuilder.CreatePartner();
            SetPartnerPromoCodeLimitRequest request = PartnerPromoCodeLimitBuilder.CreateSetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(p => p.GetByIdAsync(id))
                .ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(p => p.UpdateAsync(It.IsAny<Partner>()));

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Assert
            _partnersRepositoryMock.Verify(p => p.UpdateAsync(partner), Times.Once());
        }
    }
}