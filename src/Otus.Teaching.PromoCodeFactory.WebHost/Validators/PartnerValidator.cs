﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Validators
{
    public static class PartnerValidator
    {
        public static bool ValidatePartnerGetByIdAsync(Guid id, Partner partner)
        {
            if (partner == null)
            {
                throw new NotFoundException($"Объект с идентификатором: {id} не найден");
            }
            else if (!partner.IsActive)
            {
                throw new BadRequestException($"Партнер с идентификатором: {partner.Id} заблокирован");
            }
            return true;
        }
    }
}
