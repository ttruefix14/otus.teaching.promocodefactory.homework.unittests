﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Exceptions
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string message) : base(message)
        { }
    }
}
